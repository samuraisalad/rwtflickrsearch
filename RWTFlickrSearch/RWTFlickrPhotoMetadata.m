//
//  RWTFlickrPhotoMetadata.m
//  RWTFlickrSearch
//
//  Created by 齋藤 仁 on 2016/01/14.
//  Copyright © 2016年 Colin Eberhardt. All rights reserved.
//

#import "RWTFlickrPhotoMetadata.h"

@implementation RWTFlickrPhotoMetadata

- (instancetype)initWithNumberOfFavorites:(NSNumber*)favorites andComments:(NSNumber*)comment {
    self = [super init];
    if (self) {
        _favorites = [favorites integerValue];
        _comments = [comment integerValue];
    }
    return self;
}

- (NSString *)description {
    return [NSString stringWithFormat:@"metadata: comments=%lU, faves=%lU",
            self.comments, self.favorites];
}

@end
