//
//  RWTFlickrPhotoMetadata.h
//  RWTFlickrSearch
//
//  Created by 齋藤 仁 on 2016/01/14.
//  Copyright © 2016年 Colin Eberhardt. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RWTFlickrPhotoMetadata : NSObject

- (instancetype)initWithNumberOfFavorites:(NSNumber*)favorites andComments:(NSNumber*)comment;

@property (nonatomic) NSUInteger favorites;
@property (nonatomic) NSUInteger comments;

@end
